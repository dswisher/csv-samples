/**
 * Theme used for yield curve in Grand Central
 */

Highcharts.theme = {
    colors: ['#0066cc', '#a38a36', '#ff5001', '#317923', '#7b005b', '#ec3786'],
    chart: {
        // width: 448,
        // height: 227,
        // margin: [0, 48, 17, 0],
        plotBorderWidth: 1,
        borderRadius: 0,
        borderColor: '#ccc',
        backgroundColor: 'none',
        plotBackgroundColor: '#fff',
        style: {
            fontFamily: "Arial, Helvetica, sans-serif",
            overflow: "visible",
            fontSize: "1em",
            zIndex: 1
        }
    },
    title: {
        style: {
            display: 'none'
        }
    },
    credits: {
        enabled: false
    },
    legend: {
        enabled: false
    },
    tooltip: {
        enabled: true,
        shadow: false,
        style: {
            padding: 0,
            margin: 0,
            fontFamily: 'Arial, Helvetica, sans-serif',
            fontSize: '1em'
        },
        shared: false,
        //positioner: function (boxWidth, boxHeight, point) {
        //    if ((self.$el.find('.chart_block.yield .chart_wrap').offset().left - self.$el.offset().left + point.plotX) < 283) {
        //        self.yieldCurveTipAlign = '';
        //        self.$el.find('.hover_box.bond_info').removeClass('left');
        //    }
        //    else {
        //        self.yieldCurveTipAlign = 'left';
        //        self.$el.find('.hover_box.bond_info').addClass('left');
        //    }
        //    return {
        //        x: point.plotX,
        //        y: point.plotY
        //    };
        //},
        //formatter: function () {
        //    // If in 'time-period' compare mode, only show bond chiclet if on the main curve,
        //    // else show simple tooltip like in history chart.
        //    var dataSeriesCode = self.$el.find('.time_period_options li.base_choice').data('seriescode');
        //    if ((self.params.mode === 'time_period') && (this.point.series.options.id !== dataSeriesCode)) {
        //        return '<div class="hover_box point_data left" style="position: static"><div class="arrow"><!-- ARROW --></div><div class="bubble"><div class="hover_content"><h6 class="data_point_title">' + this.series.options.name + '</h6><h6 class="data_point_content">' + this.y.toFixed(3) + '%</h6></div></div></div>';
        //    }
        //    else {
        //        $.ajax({
        //            url: self.cfg.dataServicesHost + 'tooltip?officialId=' + this.point.id + '&entitlementToken=' + self.cfg.entitlementToken + '&ckey=' + self.cfg.ckey,
        //            dataType: "json",
        //            xhrFields: {
        //                withCredentials: false
        //            },
        //            success: function (data) {
        //                var dateTimeFormat = 'MMM D, YYYY h:mm A';
        //                if (self.$el.attr('data-localtimeformat') === '24HR') {
        //                    dateTimeFormat = 'MMM D, YYYY HH:mm';
        //                }
        //                var formattedDateTime = LocalTime.getLocalizedTimestampFromUtc(data.LastTime, '', dateTimeFormat, self.$el.attr('data-localtimelocale'), false, 0);
        //                data.LastTime = formattedDateTime;
        //                data.Last = data.Last.toFixed(3);
        //                data.NetChange = data.NetChange.toFixed(3);
        //                data.ChangePercent = data.ChangePercent.toFixed(2);
        //                data.FiftyTwoWeekLow = data.FiftyTwoWeekLow.toFixed(3);
        //                data.FiftyTwoWeekHigh = data.FiftyTwoWeekHigh.toFixed(3);
        //                data.Left = ((data.Last - data.FiftyTwoWeekLow) / (data.FiftyTwoWeekHigh - data.FiftyTwoWeekLow)) * 100;
        //                data.curveTipAlign = self.yieldCurveTipAlign;
        //                var html = self.chickletTemplate.render(data);
        //                var find = $(self.chart.container).find('div.highcharts-tooltip tspan');
        //                if (find.length > 0) {
        //                }
        //                else {
        //                    find = $(self.chart.container).find('div.highcharts-tooltip span');
        //                    find.html(html);
        //                }
        //            }
        //        });
        //        return "<div></div>";
        //    }
        //},
        //useHTML: true,
        borderWidth: 0
    },
    yAxis: {
        startOnTick: false,
        endOnTick: false,
        showLastLabel: true,
        tickInterval: 1,
        lineWidth: 0,
        gridLineColor: '#ccc',
        labels: {
            formatter: function () {
                return (this.value).toFixed(2) + "%";
            },
            style: {
                fontSize: '9px',
                color: '#333'
            },
            height: "210px",
            align: 'left',
            overflow: 'justify',
            x: 5
        },
        minPadding: 0.25,
        maxPadding: 0.10,

        opposite: true,
        title: {
            style: {
                display: 'none'
            }
        }
    }
};

// Apply the theme
var highchartsOptions = Highcharts.setOptions(Highcharts.theme);
